package com.mind.recipereview.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mind.recipereview.entity.Rating;

@Repository
public interface RatingDao  extends JpaRepository<Rating, Integer> {

}
