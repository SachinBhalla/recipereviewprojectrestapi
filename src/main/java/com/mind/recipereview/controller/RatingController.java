package com.mind.recipereview.controller;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mind.recipereview.entity.LikeAndUnlike;
import com.mind.recipereview.entity.Rating;
import com.mind.recipereview.entity.RecipeDetail;
import com.mind.recipereview.entity.User;
import com.mind.recipereview.service.IUserService;
import com.mind.recipereview.service.LikeAndUnlikeService;
import com.mind.recipereview.service.RatingService;
import com.mind.recipereview.service.RecipeService;
@RestController
@RequestMapping("/rating")
public class RatingController {
	
	@Autowired
	RatingService ratingService;
	
	@Autowired
	RecipeService recipeService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	LikeAndUnlikeService likeAndUnlikeService;
	
	@PostMapping(value = "/saverating")
	public ResponseEntity<String> saveRating(@Valid @RequestParam("recipeId") Integer recipeId,
							@RequestBody Rating rating,Authentication authentication) {
		
		int like=0;
		
		
		//  Through This We Can find recipeId And Through recipeId We Can Find picture related To That Recipe
		RecipeDetail recipeDetail=recipeService.findById(recipeId);
		
		User user = userService.findByUserName(authentication.getName());
		
		Rating theRating=new Rating();
		theRating.setRecipeId(recipeId);
		theRating.setUserId(user.getUserId());
		theRating.setRating(rating.getRating());
		theRating.setComment(rating.getComment());
		// saving the details into the database
		Rating saverating=ratingService.save(theRating);
		
		System.out.println("Rating Data is Saved");
		
		// saving the Like Count
		
		LikeAndUnlike theLikeAndUnlike=new LikeAndUnlike();
		
		LikeAndUnlike likeAndUnlike=likeAndUnlikeService.getSameRecipe(recipeId);
		if(likeAndUnlike==null) {
			throw new RuntimeException("Like and Unlike is Null in controller");
		}
		// for updating purpose
		theLikeAndUnlike.setId(likeAndUnlike.getId());
		theLikeAndUnlike.setRecipeId(recipeId);
		theLikeAndUnlike.setUserid(user.getUserId());
		
		int getRating=theRating.getRating();
		like=likeAndUnlike.getLikeCount();
		if(getRating<5) {
			System.out.println("in if "+like);
			like=like-1;
		}
		else if(like>=5) {
			System.out.println("in else"+like);
			like=like+1;
		}
		System.out.println("After Block"+like);
		theLikeAndUnlike.setLikeCount(like);
		LikeAndUnlike test=likeAndUnlikeService.save(theLikeAndUnlike);
		System.out.println("After Saving Like And Unlike The Data");
			
		return new ResponseEntity<>("Rating Is Saved Sucessfully",HttpStatus.OK);
	}
	
	

}
