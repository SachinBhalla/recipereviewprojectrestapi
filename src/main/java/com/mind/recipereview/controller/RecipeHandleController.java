package com.mind.recipereview.controller;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.condition.ProducesRequestCondition;

import com.mind.recipereview.entity.RecipeDetail;
import com.mind.recipereview.entity.User;
import com.mind.recipereview.service.IUserService;
import com.mind.recipereview.service.RecipeService;

@RestController
@RequestMapping("/recipe")
public class RecipeHandleController {
	
	@Autowired
	RecipeService recipeService;
	
	@Autowired
	IUserService userService;
	
	@GetMapping(value = "/list", produces = "application/JSON")
	public ResponseEntity<Page<RecipeDetail>> findAll(@PageableDefault(size = 10, page = 1) Pageable pageable,
													   Authentication authentication) {
		// This Will Find The Current Login UserName
		String username=authentication.getName();
		// This If Condition Applies When User Home Page Without Login
		if(username==null || username.trim().isEmpty()) {
			System.out.println("Welcome");
		}
		
		System.out.println(username);
		return ResponseEntity.ok(recipeService.findAllRecipesUsingPage(pageable));
	}
	@GetMapping(value="/samerecipe",produces = "application/JSON")
	public RecipeDetail samedsih(@RequestParam("id") Integer id) {
		RecipeDetail recipeDetail=recipeService.findById(id);
		return recipeDetail;
	}
	@PostMapping(value="/saverecipe")
	public ResponseEntity<?> saveRecipe(@RequestBody RecipeDetail recipeDetail){
		if(recipeDetail!=null) {
			recipeService.saveRecipeDetail(recipeDetail);
			return new ResponseEntity<>("Recipe Detail Is Saved Sucessfully",HttpStatus.OK);
		}
		else
			return new ResponseEntity<>("Failed To Save Recipe Detail",HttpStatus.BAD_REQUEST);
	}
	@PutMapping(value="/updateRecipe")
	public ResponseEntity<?> updateRecipe(@RequestBody RecipeDetail recipeDetail){
		if(recipeDetail!=null) {
			recipeService.saveRecipeDetail(recipeDetail);
			return new ResponseEntity<>("Recipe Detail Is update Sucessfully",HttpStatus.OK);
		}
		else
			return new ResponseEntity<>("Failed To update Recipe Detail",HttpStatus.BAD_REQUEST);
	}
	@DeleteMapping(value = "/deleterecipe")
	public ResponseEntity<?>  deleteRecipe(@RequestParam("id") Integer id){
		
		RecipeDetail recipeDetail=recipeService.findById(id);
		if(recipeDetail!=null) {
			recipeService.deleteById(id);
			return new ResponseEntity<>("Recipe is Delete Sucessfully",HttpStatus.OK);
		}
		return new ResponseEntity<>("Failed To Delte The Recipe",HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(value = "/myrecipe", produces = "application/JSON")
	public ResponseEntity<List<RecipeDetail>> myRecipe(@PageableDefault(size = 10, page = 1) Pageable pageable,
													   Authentication authentication) {
		
		User user= userService.findByUserName(authentication.getName());
		
		System.out.println("Welcome"+authentication.getName());
		
		return ResponseEntity.ok(recipeService.getRecipeByUserId(user.getUserId()));
	}
	

}
